import argparse
import pickle
from black import Cache
import pandas as pd
import numpy as np


NUMERICAL_FEATURES = ['accounts_count', 'deposit_count', 'withdrawal_count', 'trade_sum_usd', 'trade_profit_usd', 'trades_count']
CATEGORICAL_FEATURES = ['region', 'device'] 


def load_pkl(filename):
    with open(filename, 'rb') as f:
        data = pickle.load(f)
    return data


def main(val_path, cl_encoder_path, reg_encoder_path, cl_model_path, reg_model_path):
    val = pd.read_csv(val_path)
    val = val.fillna(val.median())
    cl_encoder = load_pkl(cl_encoder_path)
    reg_encoder = load_pkl(reg_encoder_path)
    reg_model = load_pkl(reg_model_path)
    cl_model = load_pkl(cl_model_path)

    #classfication
    cat_df = cl_encoder.transform(val[CATEGORICAL_FEATURES])
    X = pd.concat([cat_df, val[NUMERICAL_FEATURES]], axis=1)
    y_pred = cl_model.predict(X)
    val['prediction'] = y_pred

    # regression
    reg_df = val[val.prediction != 0]
    for name in NUMERICAL_FEATURES:
        reg_df[name] = reg_df[name].apply(lambda x: np.log1p(x))

    cat_df = reg_encoder.transform(reg_df[CATEGORICAL_FEATURES])
    X = pd.concat([cat_df, reg_df[NUMERICAL_FEATURES]], axis=1)
    y_pred = reg_model.predict(X)
    reg_df['prediction'] = y_pred

    result = reg_df[['user','prediction']].append(val[val.prediction == 0][['user', 'prediction']])
    result['prediction'] = result.prediction.apply(lambda x: 0 if x<0 else x)
    result.to_csv('prediction.csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='training script')
    parser.add_argument('path_to_val_dataset', type=str)
    parser.add_argument('cl_encoder', type=str)
    parser.add_argument('reg_encoder', type=str)
    parser.add_argument('cl_model', type=str)
    parser.add_argument('reg_model', type=str)
    args = parser.parse_args()
    val_path = args.path_to_val_dataset
    cl_encoder = args.cl_encoder
    reg_encoder = args.reg_encoder
    cl_model = args.cl_model
    reg_model = args.reg_model
    
    main(val_path, cl_encoder, reg_encoder, cl_model, reg_model)

