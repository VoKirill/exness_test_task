import argparse
import pandas as pd
import pickle
import numpy as np
from category_encoders.cat_boost import CatBoostEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error


NUMERICAL_FEATURES = ['accounts_count', 'deposit_count', 'withdrawal_count', 'trade_sum_usd', 'trade_profit_usd', 'trades_count']
CATEGORICAL_FEATURES = ['region', 'device'] 


def encode_features(df, cat_features, y_name, encoder_name):
    ce = CatBoostEncoder()
    cat_df =  ce.fit_transform(df[cat_features], df[y_name])
    with open(encoder_name, 'wb') as f:
        pickle.dump(ce, f)

    return cat_df 
    

def report_formation(X, y, model, metric, report_name):
    result = cross_val_score(model, X, y, scoring=metric, cv=5)
    print(result)
    with open(report_name, 'w') as f:
        f.write(str(result))


def train_model(X, y, model, model_name):
    model.fit(X, y)
    y_pred = model.predict(X)
    with open(model_name, 'wb') as f:
        pickle.dump(model, f)

    return y_pred


def main(train_path):
    train = pd.read_csv('train.csv')
    train = train.fillna(train.median())
    
    # classification_part
    cat_df = encode_features(train, CATEGORICAL_FEATURES, 'revenue', 'classifcation_cat_feature_encoder.pkl')
    classifcation_y = train['revenue'].apply(lambda x: 1 if x!=0 else 0)
    X = pd.concat([cat_df, train[NUMERICAL_FEATURES]], axis=1)
    report_formation(X, classifcation_y, LogisticRegression(max_iter=200),'roc_auc', 'classifcation_report.txt')

    y_pred = train_model(X, classifcation_y,LogisticRegression(max_iter=200, random_state=42), 'classification_model.pkl')
    train['classification_ans'] = y_pred  

    # regression_part
    reg_train = train[train.classification_ans == 1]
    cat_df = encode_features(reg_train, CATEGORICAL_FEATURES, 'revenue', 'regression_cat_feature_encoder.pkl')
    for name in NUMERICAL_FEATURES:
        reg_train[name] = reg_train[name].apply(lambda x: np.log1p(x))
    
    X = pd.concat([cat_df, reg_train[NUMERICAL_FEATURES]], axis=1)
    report_formation(X, reg_train.revenue, LinearRegression(), 'neg_mean_absolute_error', 'regression_report.txt')

    y_pred = train_model(X, reg_train.revenue,  LinearRegression(), 'regression_model.pkl')
    reg_train['regression_ans'] = y_pred 

    #metric calculation 
    y_true_1 = train[train['classification_ans'] == 0].revenue.to_list() 
    y_true_2 = reg_train.revenue.to_list()
    y_true = y_true_1 + y_true_2
    y_pred = [0]*len(y_true_1) + reg_train['regression_ans'].to_list()
    print(mean_absolute_error(y_true, y_pred))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='training script')
    parser.add_argument('path_to_dataset', type=str)
    args = parser.parse_args()
    train_path = args.path_to_dataset
    
    main(train_path)